! Compile using e.g. gfortran ppm.f90 example.f90
program ppm_test
use ppm
double precision :: a(100,100) = reshape([(i,i=1,100*100)], [100,100])
double precision :: a3d(30,30,30) = reshape([(i,i=1,30*30*30)], [30,30,30])

call write_ppm(a, 'data.ppm')
call write_slice_ppm(a3d, 2, 'data_slice.ppm', rainbow)
end program
